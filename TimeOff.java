
import java.util.ArrayList;
public class TimeOff {
    protected Employee employee;
    protected ArrayList<String> comments = new ArrayList<>();

    protected boolean isDeductibleFromAnnualLeave;

    protected boolean isApproved;



    TimeOff(Employee emp, String comment ){
        this.employee = emp;
        this.comments.add(comment);
    }

    public boolean isDeductibleFromAnnualLeave() {
        return isDeductibleFromAnnualLeave;
    }

    public boolean isApproved(){
        return this.isApproved;
    }


//    comments are implemented as an array
    public void appendComment(String comment){

        this.comments.add(comment);

    }

    public void approve(){
        this.isApproved = true;
    }
    public void reject(){
        this.isApproved = false;
    }


}
