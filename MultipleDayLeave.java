import static java.lang.Math.toIntExact;
public class MultipleDayLeave extends TimeOff {

    private Long timeOffStartDateUnix;
    private Long timeOffEndDateUnix;

//    constructor
    MultipleDayLeave( Employee emp, String comment, Long startDateUnixTimestamp, Long endDateUnixTimestamp){
//        parent constructor
        super(emp,comment );
        this.timeOffStartDateUnix = startDateUnixTimestamp;
        this.timeOffEndDateUnix = endDateUnixTimestamp;

        this.computeNrDaysInARequest();
    }

    public int computeNrDaysInARequest(){

        int nrOfDaysOff = toIntExact(this.timeOffEndDateUnix - this.timeOffStartDateUnix) /1000  / 3600 / 24;
        super.isDeductibleFromAnnualLeave = ( ( this.employee.getNrAnnualLeaveDays() - nrOfDaysOff ) >= 0 );
        return nrOfDaysOff;
    }

}
