public class Employee {
        private static final  int YEARLY_LEAVE_DAYS = 24;

        private String firstName = "";
        private String lastName = "";
        private String email = "";
        private  int CNP;
        private int nrAnnualLeaveDays = YEARLY_LEAVE_DAYS;

        public void Employee(String firstName, String lastName, String email, int CNP){

            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.CNP = CNP;

        }

    public int getNrAnnualLeaveDays() {
        return nrAnnualLeaveDays;
    }

    public void subtractFromNrAnnualLeaveDays(int nrOfDaysToSubtract) {

        this.nrAnnualLeaveDays = this.nrAnnualLeaveDays - nrOfDaysToSubtract;
    }

    public boolean hasDaysOfLeave(){
            return this.nrAnnualLeaveDays>0;
        }


//        every year 24 days are added to total employee days
    public void yearlyLeaveDaysAddition(){
            this.nrAnnualLeaveDays = this.nrAnnualLeaveDays + YEARLY_LEAVE_DAYS;
    }


}
