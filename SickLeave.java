import static java.lang.Math.toIntExact;

public class SickLeave extends TimeOff{
    private Long timeOffStartDate;
    private Long timeOffEndDate;
    private String explanatoryCertificate;
    private boolean isApprovedByDoctor = false;

    SickLeave(Employee emp, String comment,  Long startDateUnixTimestamp, Long endDateUnixTimestamp){
        super(emp, comment);
        this.timeOffStartDate = startDateUnixTimestamp;
        this.timeOffEndDate = endDateUnixTimestamp;
        this.computeNrDaysInARequest();
    }

    public void uploadCertificate(String certificate){
        this.isApprovedByDoctor = true;
        this.explanatoryCertificate = certificate;
    }

    public boolean isCertified(){
        return this.isApprovedByDoctor;
    }


//    compute days as well as set the "is deductible from annual leave " variable
    private int computeNrDaysInARequest(){

        int nrOfDaysOff = toIntExact(this.timeOffStartDate - this.timeOffEndDate) /1000  / 3600 / 24;
        super.isDeductibleFromAnnualLeave = ( ( this.employee.getNrAnnualLeaveDays() - nrOfDaysOff ) >= 0 );
        return nrOfDaysOff;
    }


}
