public class PartialDayLeave extends TimeOff {
//    number of days constant
    private final static int WORKING_HOURS_IN_A_DAY = 8;
    private int requestedNrHours;
    private Long timeOffDate;


//    constructor
    PartialDayLeave(Employee emp, String comment, int requestedNrHours, Long timeOffDate){
        super(emp, comment);
        this.timeOffDate = timeOffDate;
        this.requestedNrHours =  requestedNrHours;

    }

    public int computeNrUntakenHours(){
        return WORKING_HOURS_IN_A_DAY-this.requestedNrHours;
    }



}
